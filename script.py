fin = open('template.html', 'r')

car_id = "car14"
size = 6

content = ""
for line in fin.readlines():
	content += line
fin.close()

content = content.replace("TODO_ID", car_id)

items_content = ""
for i in range(size):
	item_line = '\t\t<li data-target="#' + car_id + '" data-slide-to="' + str(i) + '"'
	if i == 0:
		item_line += ' class="active"' 
	item_line += '></li>'
	items_content += item_line + '\n'

content = content.replace('TODO_NEW_ITEM', items_content)

images_content = ""
for i in range(size):
	if i == 0:
		image_line = '\t\t<div class="carousel-item active"><div class="view"><img class="d-block w-100" src="'
	else:
		image_line = '\t\t<div class="carousel-item"><div class="view"><img class="d-block w-100" src="'
	image_line += 'images/' + car_id + '/img' + str(i+1) + '.jpg">'
	image_line += '<div class="mask rgba-black-light"></div></div></div>'
	images_content += image_line + '\n'

content = content.replace("TODO_IMAGES", images_content)

print(content)